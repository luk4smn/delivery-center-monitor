<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>


Requirements from this project:

Back End
- 
- composer install
- php artisan migrete:fresh --seed
- npm install -g laravel-echo-server

To start Socket IO
- cd api\socket\laravel-echo-server\
- laravel-echo-server start

Don't forget to make Redis configuration in .env file

Redis
-
- sudo service redis-server start
- redis-cli
- monitor

Front End
- 
- nodejs v10 installed
- cd /front
- npm run watch or npm run build
