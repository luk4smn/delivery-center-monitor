<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Constants;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(StateTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(RolesTableSeeder::class);

        \App\Models\User::factory()->create([
            'name'      => 'Administrador',
            'email'     => 'admin@admin.com',
            'password'  => bcrypt("admin"),
            'city_id'   => 1292,
            'cpf'       => '920.983.920-01',
            'role_id'   => Constants::ROLE_ADMIN
        ]);

        \App\Models\User::factory()->create([
            'name'          => 'Entregador Teste',
            'email'         => 'lucas.mn@outlook.com',
            'password'      => bcrypt("delivery"),
            'address'       => 'Rua Giló Guedes',
            'address_number'=> '17',
            'address_obs'   => 'APT 303',
            'zip'           => '58406-000',
            'contact'       => '(75) 99192-6161',
            'cpf'           => '729.484.890-40',
            'role_id'       => Constants::ROLE_DELIVERY_MAN,
            'city_id'       => 1292,
        ]);

        \App\Models\VeiculoUser::create([
            'type'          => Constants::TYPE_MOTOCICLE,
            'user_id'       => 1,
            'identifier'    => 'JOJ-1254',
            'max_cubage'    => 10,
            'max_weight'    => 166.20,
        ]);

        \App\Models\VeiculoUser::create([
            'type'          => Constants::TYPE_CAR,
            'user_id'       => 2,
            'identifier'    => 'SOP-1254',
            'max_cubage'    => 10.10,
            'max_weight'    => 200.20,
        ]);

        \App\Models\CentroDistribuicao::create([
            'name'              => 'CD Campina Grande',
            'cod'               => 'CDCG',
            'fantasy_name'      => 'Delivery Center CG LTDA',
            'cnpj'              => '01.630.115-0001/22',
            'address'           => 'Rua Giló Guedes',
            'address_number'    => '17',
            'address_obs'       => null,
            'zip'               => '58406-000',
            'city_id'           => 1292
        ]);

        \App\Models\CentroDistribuicao::create([
            'name'              => 'CD João Pessoa',
            'cod'               => 'CDCG',
            'fantasy_name'      => 'Delivery Center JP LTDA',
            'cnpj'              => '01.630.115-0001/23',
            'address'           => 'Av Epitácio Pessoa',
            'address_number'    => '1276',
            'address_obs'       => null,
            'zip'               => '58030-972',
            'city_id'           => 1292
        ]);

    }
}
