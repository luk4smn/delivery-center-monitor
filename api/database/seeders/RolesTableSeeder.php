<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->insert([
            [
                'name' => 'Administrador',
                'created_at' => now()
            ],
            [
                'name' => 'Entregador Parceiro',
                'created_at' => now()
            ]
        ]);
    }

}
