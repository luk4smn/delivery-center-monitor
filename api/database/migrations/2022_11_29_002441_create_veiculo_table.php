<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('veiculo_user', function (Blueprint $table) {
            $table->id();
            $table->string('identifier')->nullable();
            $table->unsignedBigInteger('user_id')->unique();
            $table->unsignedBigInteger('type');
            $table->unsignedFloat('max_cubage');
            $table->unsignedFloat('max_weight');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('veiculo');
    }
};
