<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lote_entrega_nfe', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('nfe_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('delivery_lot_id');
            $table->string('obs')->nullable();
            $table->string('image')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('nfe_id')->references('id')->on('nfes');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('delivery_lot_id')->references('id')->on('lotes_entrega')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entrega_nfe');
    }
};
