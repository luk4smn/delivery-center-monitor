<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centros_distribuicao', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('cod');
            $table->string('fantasy_name');
            $table->string('cnpj');
            $table->string('address');
            $table->string('address_number');
            $table->string('address_obs')->nullable();
            $table->string('zip');
            $table->unsignedBigInteger('city_id');

            $table->foreign('city_id')->references('id')->on('cidades');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centros-distribuicao');
    }
};
