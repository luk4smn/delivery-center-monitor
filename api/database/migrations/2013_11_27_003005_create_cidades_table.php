<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidades', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('iso');
            $table->string('iso_ddd');
            $table->string('status');
            $table->string('slug');
            $table->string('population');
            $table->string('lat');
            $table->string('long');
            $table->string('income_per_capita');
            $table->unsignedBigInteger('state_id');

            $table->foreign('state_id')->references('id')->on('estados');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cidades');
    }
};
