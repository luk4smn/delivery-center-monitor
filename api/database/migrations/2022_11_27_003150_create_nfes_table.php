<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nfes', function (Blueprint $table) {
            $table->id();
            $table->string('note_key');
            $table->string('note_number');
            $table->string('provider');
            $table->string('provider_doc_number');
            $table->string('client');
            $table->string('client_doc_number');
            $table->string('client_contact');
            $table->string('tracking_code')->nullable();
            $table->string('delivery_address');
            $table->unsignedFloat('weight');
            $table->unsignedFloat('cubage');
            $table->unsignedBigInteger('volumes');
            $table->unsignedBigInteger('delivery_center_id');
            $table->unsignedBigInteger('status')->default(\App\Models\Constants::STATUS_PENDING);
            $table->timestamp('issued_at')->nullable();

            $table->foreign('delivery_center_id')->references('id')->on('centros_distribuicao');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nfes');
    }
};
