<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nfe_produtos', function (Blueprint $table) {
            $table->id();
            $table->string('quantity');
            $table->unsignedFloat('value');
            $table->unsignedBigInteger('nfe_id');
            $table->unsignedBigInteger('product_id');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('product_id')->references('id')->on('produtos');
            $table->foreign('nfe_id')->references('id')->on('nfes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entrada-produtos-nfe');
    }
};
