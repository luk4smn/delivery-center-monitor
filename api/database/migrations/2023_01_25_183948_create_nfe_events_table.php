<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nfe_events', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('nfe_id');
            $table->unsignedBigInteger('user_id');
            $table->string('event');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('nfe_id')->references('id')->on('nfes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nfe_events', function (Blueprint $table) {
            //
        });
    }
};
