<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lotes_entrega', function (Blueprint $table) {
            $table->id();
            $table->string('lot_name')->nullable();
            $table->string('obs')->nullable();
            $table->unsignedTinyInteger('status')->default(\App\Models\Constants::STATUS_PENDING);
            $table->unsignedBigInteger('delivery_center_id');
            $table->unsignedBigInteger('user_id');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('delivery_center_id')->references('id')->on('centros_distribuicao');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lotes_entrega', function (Blueprint $table) {
            //
        });
    }
};
