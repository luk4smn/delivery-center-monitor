<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('address')->nullable();
            $table->string('address_number')->nullable();
            $table->string('address_obs')->nullable();
            $table->string('zip')->nullable();
            $table->string('contact')->nullable()->default("NÃO INFORMADO");
            $table->string('cpf')->nullable();

            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('role_id');

            $table->foreign('city_id')->references('id')->on('cidades');
            $table->foreign('role_id')->references('id')->on('roles');

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
