<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers as Controllers;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::controller(Controllers\AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');
});

Route::get('roles', [Controllers\UserController::class, 'roles']);
Route::get('cities', [Controllers\CityStateController::class, 'cities']);
Route::get('states', [Controllers\CityStateController::class, 'states']);
Route::get('state-of-my-city/{id}', [Controllers\CityStateController::class, 'stateOfMyCity']);
Route::get('get-my-user', [Controllers\UserController::class, 'getMyUser']);
Route::put('update-my-user', [Controllers\UserController::class, 'updateMyUser']);
Route::get('delivery-lot/my-deliveries', [Controllers\LoteEntregaController::class, 'myDeliveries']);
Route::post('delivery-lot/my-deliveries/note/{id}/start', [Controllers\LoteEntregaController::class, 'startNoteDelivery']);
Route::post('delivery-lot/my-deliveries/note/{id}/finish', [Controllers\LoteEntregaController::class, 'finishNoteDelivery']);
Route::post('users/{id}/store-vehicle', [Controllers\UserController::class, 'storeVehicle']);
Route::post('delivery-center/{id}/import-nfe', [Controllers\CentroDistribuicaoController::class, 'importNotes']);
Route::post('notify-localization', [Controllers\EventsController::class, 'notifyLocalization']);
Route::post('tracking', [Controllers\TrackingController::class, 'index']);

Route::get('my-report', [Controllers\ReportController::class, 'myReport']);
Route::get('full-report', [Controllers\ReportController::class, 'fullReport']);

// CRUD ROUTES
Route::resources([
    'users'           => Controllers\UserController::class,
    'delivery-center' => Controllers\CentroDistribuicaoController::class,
    'notes'           => Controllers\NfeController::class,
    'delivery-lot'    => Controllers\LoteEntregaController::class,
]);

