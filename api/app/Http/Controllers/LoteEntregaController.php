<?php

namespace App\Http\Controllers;

use App\Events\LocalizationEvent;
use App\Models\CentroDistribuicao;
use App\Models\Constants;
use App\Models\LoteEntrega;
use App\Models\LoteEntregaNfe;
use App\Models\Nfe;
use Illuminate\Http\Request;

class LoteEntregaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request, LoteEntrega $model)
    {
        if($search = $request->input('q', null)) {
            $model = $model->search($search,['lot_name','status_id', 'client', 'client_doc_number'])
                ->orderBy('status','asc')
                ->get();
        }
        else if($search = $request->input('c', null)) {
            $model = $model->search($search,['delivery_center_id'])
                ->orderBy('status','asc')
                ->get();
        }
        else{
            $model = $model::all();
        }

        return $this->getDeliveryLotData($model);
    }

    public function show($id)
    {
        $model = LoteEntrega::findOrFail($id);

        $collection = collect();

        $collection->push($model);

       return $this->getDeliveryLotData($collection)->first();
    }

    /**
     * @throws \Exception
     */
    public function store(Request $request, LoteEntrega $model)
    {
        $request->validate([
            'selected_notes'  => 'required|array',
            'delivery_center' => 'required|numeric'
        ]);

        $notes_ids      = $request->input('selected_notes');
        $center_id      = $request->input('delivery_center');
        $deliveryCenter = CentroDistribuicao::findOrFail($center_id);
        $user           = auth()->user();
        $userMaxWeight  = $user->vehicle->max_weight ?? 0;
        $userMaxCubage  = $user->vehicle->max_cubage ?? 0;

        // verificar se tem lote não finalizado, se tiver, não avança
        if(!$user->lots->where('status', '<=', Constants::STATUS_SUSPENDED)->count()){
            // verificar a soma do peso das notas, se exceder não avança
            $totals = Nfe::getTotalWeightCubageAndVolumes($notes_ids);

            if($userMaxWeight >= $totals->total_weight || $userMaxCubage >= $totals->total_cubage){
                $deliveryLot = $model->create([
                    'user_id'            => auth()->user()->id,
                    'status'             => Constants::STATUS_WITHDRAWN,
                    'delivery_center_id' => $deliveryCenter->id
                ]);

                $deliveryLot->lot_name = "LOTE-"
                    .$deliveryCenter->cod.'-'
                    .auth()->user()->id.'-'
                    .$deliveryLot->id;

                $deliveryLot->save();
                $message = 'NF-e Associada ao lote: '.$deliveryLot->lot_name.' pelo entregador '. auth()->user()->name;

                try {
                    foreach ($notes_ids as $note_id){
                        $note = Nfe::findOrFail($note_id);
                        $deliveryLot->items()->create([
                            'nfe_id'    => $note->id,
                            'user_id'   => auth()->user()->id,
                            'obs'       => null
                        ]);

                        $note->status = Constants::STATUS_WITHDRAWN;
                        $note->events()->create([
                            'user_id'=> auth()->user()->id,
                            'event' => $message
                        ]);
                        $note->save();
                    }
                }catch (\Exception $e){
                    $deliveryLot->delete();
                    throw $e;
                }
            }else{
                throw new \Exception("Notas exedem peso ou cubagem total do veículo, verifique os dados do usuário.");
            }
        }else{
            throw new \Exception("Usuário já possui Lotes pendentes");
        }
        return response($model);
    }

    public function update(Request $request, LoteEntrega $model)
    {

        $model->update($request->all());

        return response($model);
    }

    public function destroy($id)
    {
        $model = LoteEntrega::findOrFail($id);

        if($model) {
            $model->items->map(function ($item) use ($model) {
                $note = $item->nfe;
                $note->status = Constants::STATUS_PENDING;
                $note->events()->create([
                    'user_id'   => auth()->user()->id,
                    'event' => 'NF-e Removida do lote: '.$model->lot_name.' pelo entregador '. auth()->user()->name
                ]);
                $note->save();
            });

            $model->delete();
        }

        return response("Deletado com sucesso");
    }

    public function myDeliveries(){
        $user = auth()->user();
        return $this->getDeliveryLotData($user->lots);
    }

    /**
     * @throws \Exception
     */
    public function startNoteDelivery(Request $request, $id){
        $request->validate([
           'latitude'  => 'required',
           'longitude' => 'required',
        ]);

        $user = auth()->user();

        $nfes_em_andamento = LoteEntregaNfe::countNotesByStatus(Constants::STATUS_IN_PROGRESS);

        if(!$nfes_em_andamento) {
            $nfe = Nfe::findOrFail($id);
            if($nfe->delivery->user_id == $user->id && $nfe->status < Constants::STATUS_IN_PROGRESS){
                $nfe->status = Constants::STATUS_IN_PROGRESS;
                $nfe->delivery->lot->status = Constants::STATUS_IN_PROGRESS;
                $nfe->save();
                $nfe->delivery->lot->save();

                $latitude  =  $request->input('latitude') ?? null;
                $longitude =  $request->input('longitude') ?? null;

                $nfe->delivery->localization()->create([
                    'latitude'  => $latitude,
                    'longitude' => $longitude,
                    'user_id'   => $user->id
                ]);

                $nfe->events()->create([
                    'event'     => "NF-e em rota de entrega",
                    'user_id'   => $user->id
                ]);

                event(new LocalizationEvent(
                    array(
                        'image'     => $nfe->delivery->image,
                        'latitude'  => $latitude,
                        'longitude' => $longitude,
                        'user'      => $user,
                        'note'      => $nfe,
                    )
                ));

            }else{
                throw new \Exception("Erro ao iniciar entrega");
            }
        }else{
            throw new \Exception("Já existe uma entrega em andamento");
        }

        return response("Entrega da NF-e ". $nfe->note_number ." Iniciada");
    }

    /**
     * @throws \Exception
     */
    public function finishNoteDelivery(Request $request, $id){
        $request->validate([
            'latitude'      => 'required',
            'longitude'     => 'required',
            'statusNumber'  => 'required|numeric|min:4',
            'file'          => 'required|mimes:jpg,jpeg,bmp,png',
            'observations'  => 'nullable'
        ]);

        $user = auth()->user();
        $nfe = Nfe::findOrFail($id);
        $status = $request->input('statusNumber') ?? null;
        $file = $request['file'] ?? null;
        $loteEntregaNfe = $nfe->delivery;
        $loteEntrega = $loteEntregaNfe->lot;

        if($loteEntregaNfe->user_id == $user->id && $nfe->status == Constants::STATUS_IN_PROGRESS){

            if($loteEntregaNfe->countInProgress() == 1 ?? false){
                $nfes_suspensas = LoteEntregaNfe::countNotesByStatus(Constants::STATUS_SUSPENDED);
                $loteEntrega->status = $nfes_suspensas > 0 ? Constants::STATUS_SUSPENDED: $status;
                $loteEntrega->save();
            }

            $nfe->status = $status;
            $observations = $request->input('observations') ?? null;
            $event = $status == Constants::STATUS_SUSPENDED ? "Entrega suspensa ou com pendências\n".$observations : "Entrega Finalizada";

            $latitude  =  $request->input('latitude') ?? null;
            $longitude =  $request->input('longitude') ?? null;

            $localization = $nfe->delivery->localization;
            $localization->latitude  = $latitude;
            $localization->longitude = $longitude;
            $localization->save();

            event(new LocalizationEvent(
                array(
                    'image'     => $nfe->delivery->image,
                    'latitude'  => $latitude,
                    'longitude' => $longitude,
                    'user'      => $user,
                    'note'      => $nfe,
                )
            ));

            $loteEntregaNfe->obs = $event;
            $loteEntregaNfe->setImage($file);
            $loteEntregaNfe->save();

            $nfe->save();
            $nfe->events()->create([
                'event'     => $event,
                'user_id'   => auth()->user()->id
            ]);
        }else{
            throw new \Exception("Erro ao finalizar entrega");
        }

        return response("Entrega da NF-e ". $nfe->note_number ." Finalizada");
    }

    private function getDeliveryLotData($model){
        $model->each(function ($lot){

            $lot->items->map(function ($item) {
                return $item->nfe;
            });

            $lot->notes_ids = $lot->items->pluck('id')->toArray();
            $lot->totals = Nfe::getTotalWeightCubageAndVolumes($lot->notes_ids);
            $lot->quantity_nfe = $lot->items->count();
            $lot->vehicleType = Constants::getTypeNameById($lot->user->vehicle->type);
            $lot->center;

        });

        return $model;
    }
}
