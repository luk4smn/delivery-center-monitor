<?php

namespace App\Http\Controllers;

use App\Models\Cidade;
use App\Models\Estado;
use Illuminate\Http\Request;

class CityStateController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function cities(Request $request, Cidade $model)
    {
        if($search = $request->input('q', null)) {
            $model = $model->search($search,['id','title'])->get();
        }else{
            $model = $model::with('state:id')->get();

            $all = $model->map(function ($city) {
                return $city->only(['id', 'title', 'state']);
            });

            return $all;
        }

        return $model;
    }

    public function stateOfMyCity(Cidade $id)
    {
       return $id->state->id;
    }

    public function states(Request $request, Estado $model)
    {
        if($search = $request->input('q', null)) {
            $model = $model->search($search,['id','title']);
        }else{
            $model = $model::all();
        }

        return $model->pluck('title', 'id');
    }

}
