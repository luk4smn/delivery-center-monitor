<?php

namespace App\Http\Controllers;

use App\Events\UserRegistered;
use App\Http\Requests\UserRequest;
use App\Models\Constants;
use App\Models\Role;
use App\Models\User;
use App\Models\VeiculoUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request, User $model)
    {
        if($search = $request->input('q', null)) {
            $model = $model->search($search,['name','email'])->get();
        }else{
            $model = $model::all();
        }

        return $model;
    }

    public function show($id)
    {
        $model = User::findOrFail($id);

        return $this->formatUserData($model);
    }

    public function store(UserRequest $request, User $model)
    {
        $model->create($request->merge(['password' => Hash::make($request->get('password'))])->all());

        //esse evento sincrono tem que ser registrado no EventServiceProvider
        event(new UserRegistered($model, $request->get('password')));

        return response($model);
    }

    public function update($id, UserRequest $request)
    {
        $hasPassword = $request->get('password');

        $model = User::findOrFail($id);

        $model->fill(
            $request->merge([
                'password' => Hash::make($request->get('password'))
            ])->except([$hasPassword ? '' : 'password'])
        );

        $model->update();

        return response($model);
    }

    public function destroy($id)
    {
        if ($id == auth()->user()->id) {
            throw new \Exception("Não é possível apagar o proprio usário!", 400);
        }

        $model = User::findOrFail($id);

        $model->secureDelete('lots', 'delivery');

        return response("Deletado com sucesso");
    }


    public function storeVehicle($user_id, Request $request){
        $request->validate([
            'type'          => 'required|numeric',
            'user_id'       => 'required|numeric',
            'max_cubage'    => 'required|numeric|min:1',
            'max_weight'    => 'required|numeric|min:1',
        ]);

        $model = VeiculoUser::updateOrCreate(
            ['id' => $request->get('id') ?? null],
            $request->all()
        );

        return response($model);
    }

    public function getMyUser()
    {
        $id = auth()->user()->id;

        $model = User::findOrFail($id);

        return $this->formatUserData($model);
    }

    public function updateMyUser(UserRequest $request)
    {
        $hasPassword = $request->get('password');

        $id = auth()->user()->id;
        $model = User::findOrFail($id);
        $model->fill(
            $request->merge([
                'password' => Hash::make($request->get('password'))
            ])->except([$hasPassword ? '' : 'password'])
        );

        $model->update();

        return response($model);
    }

    public function formatUserData($model){
        $model->state_id = $model->city->state->id;
        $model->role = $model->role != null ? $model->role : new Role();
        $model->roleName = $model->role->name;

        $model->vehicle = $model->vehicle != null ? $model->vehicle : new VeiculoUser();
        $model->vehicle->max_cubage = number_format($model->vehicle->max_cubage ?? 0, 2, '.', '');
        $model->vehicle->max_weight = number_format($model->vehicle->max_weight ?? 0, 2, '.', '');
        $model->vehicle->typesArr = Constants::getAllTypes();
        $model->setRelation('vehicle', $model->vehicle);

        return $model;
    }

    public function roles(Request $request, Role $model)
    {
        if($search = $request->input('q', null)) {
            $model = $model->search($search,['id','name'])->get();
        }else{
            $model = $model::all();
        }

        return $model->pluck('name', 'id');
    }



}
