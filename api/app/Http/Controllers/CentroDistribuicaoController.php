<?php

namespace App\Http\Controllers;

use App\Http\Requests\CentroDistribuicaoRequest;
use App\Models\CentroDistribuicao;
use App\Models\Nfe;
use App\Models\NfeProduto;
use App\Models\Produto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CentroDistribuicaoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request, CentroDistribuicao $model)
    {
        if($search = $request->input('q', null)) {
            $model = $model->search($search,['name','cod', 'fantasy_name']);
        }else{
            $model = $model::all();
        }

        return $model;
    }

    public function show($id)
    {
        $model = CentroDistribuicao::findOrFail($id);

        $model->state_id = $model->city->state->id;

        return $model;
    }

    public function store(CentroDistribuicaoRequest $request, CentroDistribuicao $model)
    {
        $model->create($request->all());

        return response($model);
    }

    public function update(CentroDistribuicaoRequest $request, CentroDistribuicao $model)
    {

        $model->update($request->all());

        return response($model);
    }

    public function destroy($id)
    {
        $model = CentroDistribuicao::findOrFail($id);

        $model->secureDelete('notes');

        return response("Deletado com sucesso");
    }


    public function importNotes($id, Request $request){
        $request->validate([
            'files' => 'required'
        ]);

        $deliveryCenter = CentroDistribuicao::findOrFail($id);
        $data = $request['files'];
        $output = [];

        foreach($data ?? [] as $key => $item){
            $filePath = $item->getRealPath();

            $mime = mime_content_type($filePath);

            if($mime == 'text/xml'){
                $file = file_get_contents($filePath);

                $xml = simplexml_load_string($file, "SimpleXMLElement", LIBXML_NOCDATA);
                $json = json_encode($xml->NFe->infNFe);
                $array = json_decode($json,TRUE);

                foreach ($array['det'] ?? [] as $index => $details){
                    if($details['prod'] ?? false){
                        $output['xml'][$key]['note_items'][$index] = $details['prod'] ?? '';
                    }
                }

                $output['xml'][$key]['note_key']            = $array['@attributes']['Id'];                              //chave
                $output['xml'][$key]['provider']            = $array['emit'] ?? '';                                     //dados do emissor
                $output['xml'][$key]['shipping']            = $array['transp'] ?? '';                                   //dados da transportadora - volumes - peso transp[vol]
                $output['xml'][$key]['recipient']           = $array['dest'] ?? '';                                     //dados do destinatário
                $output['xml'][$key]['delivery_address']    = $array['entrega'] ?? $array['dest']['enderDest'] ?? '';   //dados do endereço de entrega
                $output['xml'][$key]['identifier']          = $array['ide'] ?? '';                                      //dados gerais da nota nNF, dhEmi
                $output['xml'][$key]['note_value']          = $array['total']['ICMSTot']['vNF'];                        //valor total da nota

                if($array['det']['prod'] ?? false){
                    $output['xml'][$key]['note_items'][0] = $array['det']['prod'] ?? '';
                }
            }
            else{
                throw new \Exception('Tipo de arquivo '.$mime.' não aceito para essa operação',400);
            }
        }

        foreach ($output['xml'] as $xml){
            $collection = collect($xml)->recursive();

            Validator::make(
                ['note_key' => $collection->note_key ?? "",],
                ['note_key' => [Rule::unique((new Nfe())->getTable())->whereNull('deleted_at')]],
                ['unique' => 'Chave da nf-e '.$collection->identifier->nNF.' já existe no banco de dados, abortando operação']
            )->validate();

            $note = Nfe::create(
                [
                    'note_key'            => $collection->note_key ?? "",
                    'note_number'         => trim($collection->identifier->nNF ?? ""),
                    'provider'            => $collection->provider->xNome ?? "",
                    'provider_doc_number' => trim($collection->provider->CNPJ ?? ""),
                    'client'              => $collection->recipient->xNome ?? "",
                    'client_doc_number'   => $collection->recipient->CPF ?? "",
                    'client_contact'      => isset($collection->recipient->enderDest['fone']) ? trim($collection->recipient->enderDest['fone']) : "NÃO INFORMADO",
                    'delivery_address'    => $this->getAddress($collection->delivery_address) ?? "",
                    'issued_at'           => $collection->identifier->dhEmi ?? "",
                    'weight'              => $collection->shipping->vol['pesoL'] ?? "1",
                    'cubage'              => round(mt_rand(1, 2) / mt_rand(3 ,10), 2),
                    'volumes'             => ($collection->shipping->vol['qVol'] > 0 ? $collection->shipping->vol['qVol'] : 1 ) ?? "1",
                    'delivery_center_id'  => $deliveryCenter->id ?? ""
                ]
            );

            $note->tracking_code = $this->getTrackingCode($deliveryCenter, $note);
            $note->save();

            foreach ($collection->note_items as $key => $item){
                $item = collect($item)->recursive();

                $item->cEAN = $item->cEAN == "SEM GTIN" ? $item->cEAN .= "-".$key ."-".$note->id : $item->cEAN;

                $produto = Produto::updateOrCreate(
                    ['ean' => $item->cEAN],
                    ['description' => $item->xProd ?? "", 'unity' => $item->uCom ?? "", 'ref' => $item->cProd ?? ""]
                );

                $note->products()->create([
                    'product_id' => $produto->id,
                    'quantity'   => $item->qCom,
                    'value'      => $item->vUnCom
                ]);
            }

            $note->events()->create([
                'event'     => "Entrada no centro de distribuição ". $deliveryCenter->name,
                'user_id'   => auth()->user()->id
            ]);
        }

        return response("Arquivo(s) importado(s) com sucesso");

    }

    private function getAddress($deliveryAddress){
        return  ($deliveryAddress['xLgr'] ?? " ").', '.
                ($deliveryAddress['nro'] ?? " ").', '.
                ($deliveryAddress['xCpl'] ?? " ").', '.
                ($deliveryAddress['xBairro'] ?? " ").', '.
                ($deliveryAddress['xMun'] ?? " ") .' - '.
                ($deliveryAddress['UF'] ?? " ");
    }

    private function getTrackingCode($deliveryCenter, $note){
        return $deliveryCenter->cod ."-". $note->id ."-". $note->note_number;
    }





}
