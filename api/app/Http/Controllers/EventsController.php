<?php

namespace App\Http\Controllers;

use App\Events\LocalizationEvent;
use App\Models\Constants;
use App\Models\LoteEntregaNfe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function notifyLocalization(Request $request)
    {
        $request->validate([
            'latitude'  => 'required|string',
            'longitude' => 'required|string',
        ]);

        $latitude  = $request->input('latitude' , false);
        $longitude = $request->input('longitude' , false);
        $user = auth()->user();

        $loteEntregaNfe = LoteEntregaNfe::select('lote_entrega_nfe.*')
            ->join('nfes', 'nfes.id', '=', 'lote_entrega_nfe.nfe_id')
            ->where('nfes.status', Constants::STATUS_IN_PROGRESS)
            ->where('lote_entrega_nfe.user_id', $user->id)
            ->get()
            ->first();

        if($loteEntregaNfe){
            $localization = $loteEntregaNfe->localization;
            $events = $loteEntregaNfe->nfe->events;
            $city   = $user->city;

            if($latitude && $longitude){
                $localization->latitude = $latitude;
                $localization->longitude = $longitude;
                $localization->save();
            }

            event(new LocalizationEvent(
                array(
                    'image'     => $loteEntregaNfe->image,
                    'latitude'  => $latitude,
                    'longitude' => $longitude,
                    'user'      => $user,
                    'note'      => $loteEntregaNfe->nfe,
                )
            ));

            return response('Evento disparado com sucesso',200);
        }else{
            return abort(404, "Não há lote em progresso no momento");
        }

    }
}
