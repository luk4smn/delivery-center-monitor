<?php

namespace App\Http\Controllers;

use App\Models\Constants;
use App\Models\LoteEntregaNfe;
use Illuminate\Http\Request;

class TrackingController extends Controller
{

    public function index(Request $request)
    {
        $request->validate([
            'tracking_code'     => 'required_without:client_doc_number|string|nullable',
            'client_doc_number' => 'required_without:tracking_code|string|nullable',
            'note_number'       => 'required_with:client_doc_number|string|nullable',
        ]);

        $trackingCode    = $request->input('tracking_code' , null);
        $clientDocNumber = $request->input('client_doc_number' , null);
        $noteNumber      = $request->input('note_number' , null);
        $loteEntregaNfe  = null;


        if($trackingCode){
            $loteEntregaNfe = LoteEntregaNfe::select('lote_entrega_nfe.*')
                ->join('nfes', 'nfes.id', '=', 'lote_entrega_nfe.nfe_id')
                ->where('nfes.tracking_code', $trackingCode)
                ->get()
                ->first();
        }elseif ($clientDocNumber && $noteNumber){
            $loteEntregaNfe = LoteEntregaNfe::select('lote_entrega_nfe.*')
                ->join('nfes', 'nfes.id', '=', 'lote_entrega_nfe.nfe_id')
                ->where('nfes.client_doc_number', $clientDocNumber)
                ->where('nfes.note_number', $noteNumber)
                ->get()
                ->first();
        }

        if($loteEntregaNfe){
            $localization = $loteEntregaNfe->localization;
            $events = $loteEntregaNfe->nfe->events;
            $city   = $loteEntregaNfe->user->city;

            if ($localization) {
                $latitude = $localization->latitude;
                $longitude = $localization->longitude;
            } else {
                $latitude = $loteEntregaNfe->user->city->lat;
                $longitude = $loteEntregaNfe->user->city->long;
            }


            return response()->json([
                'image'     => $loteEntregaNfe->image,
                'latitude'  => $latitude,
                'longitude' => $longitude,
                'user'      => $loteEntregaNfe->user,
                'note'      => $loteEntregaNfe->nfe,

            ]);
        }else{
            return abort(404, "Não localizado");
        }

    }

}
