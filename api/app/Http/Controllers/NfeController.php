<?php

namespace App\Http\Controllers;

use App\Models\Nfe;
use Illuminate\Http\Request;

class NfeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request, Nfe $model)
    {
        if($search = $request->input('q', null)) {
            $model = $model->search($search,['number_note','provider', 'client', 'client_doc_number'])
                ->orderBy('status','asc')
                ->get();
        }
        else if($search = $request->input('c', null)) {
            $model = $model->search($search,['delivery_center_id'])
                ->orderBy('status','asc')
                ->get();
        }
        else{
            $model = $model::all();
        }

        return $model;
    }

    public function show($id)
    {
        $model = Nfe::findOrFail($id);

        $model->products->map(function ($item) {
            return $item->product;
        });

        return $model;
    }


    public function destroy($id)
    {
        $model = Nfe::findOrFail($id);

        if($model) {
            $products = $model->products;
            $events   = $model->events;

            $model->secureDelete('delivery');

            $products->each(function ($item){
                $item->delete();
            });

            $events->each(function ($item){
                $item->delete();
            });
        }

        return response("Deletado com sucesso");
    }






}
