<?php

namespace App\Http\Controllers;

use App\Models\Constants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function myReport(Request $request)
    {
        $months = range(1, 12);

        $data = DB::table(DB::raw('(
            SELECT 1 as month_num UNION
            SELECT 2 UNION
            SELECT 3 UNION
            SELECT 4 UNION
            SELECT 5 UNION
            SELECT 6 UNION
            SELECT 7 UNION
            SELECT 8 UNION
            SELECT 9 UNION
            SELECT 10 UNION
            SELECT 11 UNION
            SELECT 12
        ) as months'))
                    ->leftJoin(DB::raw('(
                SELECT
                    MONTH(DATE_FORMAT(lote.updated_at, "%Y-%m-%d")) as month_num,
                    COUNT(*) as quantidade
                FROM lotes_entrega lote
                inner join lote_entrega_nfe len on lote.id = len.delivery_lot_id
                inner join nfes n on n.id = len.nfe_id
                WHERE lote.user_id = '.auth()->user()->id.'
                AND n.status = '.Constants::STATUS_COMPLETED.'
                GROUP BY month_num
            ) as lot'), 'months.month_num', '=', 'lot.month_num')
                    ->whereIn('months.month_num', $months)
                    ->orderBy('months.month_num')
                    ->selectRaw('
                months.month_num,
                CASE
                    WHEN months.month_num = 1 THEN "Jan"
                    WHEN months.month_num = 2 THEN "Fev"
                    WHEN months.month_num = 3 THEN "Mar"
                    WHEN months.month_num = 4 THEN "Abr"
                    WHEN months.month_num = 5 THEN "Mai"
                    WHEN months.month_num = 6 THEN "Jun"
                    WHEN months.month_num = 7 THEN "Jul"
                    WHEN months.month_num = 8 THEN "Ago"
                    WHEN months.month_num = 9 THEN "Set"
                    WHEN months.month_num = 10 THEN "Out"
                    WHEN months.month_num = 11 THEN "Nov"
                    WHEN months.month_num = 12 THEN "Dez"
                END as mes,
                IFNULL(lot.quantidade, 0) as quantidade
            ')
            ->get();

        return $data;
    }

    public function fullReport(Request $request)
    {
        $months = range(1, 12);

        $data = DB::table(DB::raw('(
            SELECT 1 as month_num UNION
            SELECT 2 UNION
            SELECT 3 UNION
            SELECT 4 UNION
            SELECT 5 UNION
            SELECT 6 UNION
            SELECT 7 UNION
            SELECT 8 UNION
            SELECT 9 UNION
            SELECT 10 UNION
            SELECT 11 UNION
            SELECT 12
        ) as months'))
            ->leftJoin(DB::raw('(
                SELECT
                    MONTH(DATE_FORMAT(lote.updated_at, "%Y-%m-%d")) as month_num,
                    COUNT(*) as quantidade
                FROM lotes_entrega lote
                inner join lote_entrega_nfe len on lote.id = len.delivery_lot_id
                inner join nfes n on n.id = len.nfe_id
                WHERE n.status = '.Constants::STATUS_COMPLETED.'
                GROUP BY month_num
            ) as lot'), 'months.month_num', '=', 'lot.month_num')
            ->whereIn('months.month_num', $months)
            ->orderBy('months.month_num')
            ->selectRaw('
                months.month_num,
                CASE
                    WHEN months.month_num = 1 THEN "Jan"
                    WHEN months.month_num = 2 THEN "Fev"
                    WHEN months.month_num = 3 THEN "Mar"
                    WHEN months.month_num = 4 THEN "Abr"
                    WHEN months.month_num = 5 THEN "Mai"
                    WHEN months.month_num = 6 THEN "Jun"
                    WHEN months.month_num = 7 THEN "Jul"
                    WHEN months.month_num = 8 THEN "Ago"
                    WHEN months.month_num = 9 THEN "Set"
                    WHEN months.month_num = 10 THEN "Out"
                    WHEN months.month_num = 11 THEN "Nov"
                    WHEN months.month_num = 12 THEN "Dez"
                END as mes,
                IFNULL(lot.quantidade, 0) as quantidade
            ')
            ->get();

        return $data;
    }

}
