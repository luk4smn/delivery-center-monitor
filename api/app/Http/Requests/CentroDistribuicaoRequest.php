<?php

namespace App\Http\Requests;

use App\Models\CentroDistribuicao;
use App\Models\Constants;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CentroDistribuicaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::user()->role_id === Constants::ROLE_ADMIN) {
            return true;
        }

        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => ['required', 'min:3'],
            'cod'       => ['required', 'max:4', Rule::unique((new CentroDistribuicao)->getTable())->ignore($this->route()->user->id ?? null)->whereNull('deleted_at')],
            'cnpj'      => ['required', 'min:18', Rule::unique((new CentroDistribuicao)->getTable())->ignore($this->route()->user->id ?? null)->whereNull('deleted_at')],
            'address'   => ['required'],
            'zip'       => ['required'],
            'city_id'   => ['required'],
            'address_number'   => ['required'],
        ];
    }

}
