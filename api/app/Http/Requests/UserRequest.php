<?php

namespace App\Http\Requests;

use App\Models\Constants;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::user()->role_id === Constants::ROLE_ADMIN) {
            return true;
        }

        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => ['required', 'min:3'],
            'email' => ['required', 'email', Rule::unique((new User)->getTable())->ignore($this->route()->user ?? (auth()->id() ?? null) )->whereNull('deleted_at')],
            'cpf'   => ['required', 'min:14', Rule::unique((new User)->getTable())->ignore($this->route()->user ?? (auth()->id() ?? null) )->whereNull('deleted_at')],
            'password' => [($this->route()->user || auth()->id()) ? 'nullable' : 'required', 'confirmed', 'min:6']
        ];
    }
}

