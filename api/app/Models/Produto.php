<?php

namespace App\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produto extends Model
{
    use Searchable, SoftDeletes;

    protected $table = 'produtos';

    protected $fillable = [
        'id',
        'ean',
        'ref',
        'description',
        'unity'
    ];

}
