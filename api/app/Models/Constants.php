<?php

namespace App\Models;

class Constants
{

    const
        ROLE_ADMIN         = 1,
        ROLE_DELIVERY_MAN  = 2;

    const
        STATUS_PENDING     = 1,
        STATUS_WITHDRAWN   = 2,
        STATUS_IN_PROGRESS = 3,
        STATUS_SUSPENDED   = 4,
        STATUS_COMPLETED   = 5;

    const
        TYPE_CAR        = 1,
        TYPE_TRUCK      = 2,
        TYPE_MOTOCICLE  = 3;


    protected static $noteStatus = [
        self::STATUS_PENDING        => 'Aguardando Retirada',
        self::STATUS_WITHDRAWN      => 'Retirado do Centro de Distribuição',
        self::STATUS_IN_PROGRESS    => 'Em rota de entrega',
        self::STATUS_SUSPENDED      => 'Entrega suspensa',
        self::STATUS_COMPLETED      => 'Entrega efetuada'
    ];

    protected static $types = [
        self::TYPE_CAR          => 'Carro/Van',
        self::TYPE_TRUCK        => 'Caminhão',
        self::TYPE_MOTOCICLE    => 'Moto'
    ];

    public static function getStatusNameById($i){
        return self::$status[$i];
    }

    public static function getTypeNameById($i){
        return self::$types[$i];
    }

    public static function getAllTypes(){
        return self::$types;
    }

}
