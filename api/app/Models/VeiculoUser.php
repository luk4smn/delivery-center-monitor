<?php

namespace App\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VeiculoUser extends Model
{
    use Searchable, SoftDeletes;

    protected $table = 'veiculo_user';

    protected $fillable = [
        'id',
        'type',
        'user_id',
        'identifier',
        'max_cubage',
        'max_weight',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }



}
