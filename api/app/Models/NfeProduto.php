<?php

namespace App\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NfeProduto extends Model
{
    use Searchable, SoftDeletes;

    protected $table = 'nfe_produtos';

    protected $fillable = [
        'id',
        'product_id',
        'quantity',
        'value',
        'nfe_id',
    ];


    public function nfe()
    {
        return $this->belongsTo(Nfe::class, 'nfe_id');
    }

    public function product(){
        return $this->belongsTo(Produto::class, 'product_id');
    }


}
