<?php

namespace App\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoteEntrega extends Model
{
    use Searchable, SoftDeletes;

    protected $table = 'lotes_entrega';

    protected $fillable = [
        'id',
        'lot_name',
        'user_id',
        'status',
        'obs',
        'delivery_center_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function center()
    {
        return $this->belongsTo(CentroDistribuicao::class, 'delivery_center_id', 'id');
    }

    public function items()
    {
        return $this->hasMany(LoteEntregaNfe::class, 'delivery_lot_id');
    }



}
