<?php

namespace App\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cidade extends Model
{
    use Searchable, SoftDeletes;

    protected $table = 'cidades';

    protected $fillable = [
        'id',
        'title',
        'iso',
        'iso_ddd',
        'status',
        'slug',
        'population',
        'lat',
        'long',
        'income_per_capita',
        'state_id'
    ];

    public function state()
    {
        return $this->belongsTo(Estado::class, 'state_id', 'id');
    }



}
