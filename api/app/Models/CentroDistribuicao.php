<?php

namespace App\Models;

use App\Traits\Searchable;
use App\Traits\SecureDelete;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CentroDistribuicao extends Model
{
    use Searchable, SoftDeletes, SecureDelete;

    protected $table = 'centros_distribuicao';

    protected $fillable = [
        'id',
        'name',
        'cod',
        'fantasy_name',
        'cnpj',
        'address',
        'address_number',
        'address_obs',
        'zip',
        'city_id'
    ];

    public function city()
    {
        return $this->belongsTo(Cidade::class, 'city_id', 'id');
    }

    public function notes()
    {
        return $this->hasMany(Nfe::class, 'delivery_center_id');
    }

}
