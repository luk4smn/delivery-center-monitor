<?php

namespace App\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estado extends Model
{
    use Searchable, SoftDeletes;

    protected $table = 'estados';

    protected $fillable = [
        'id',
        'title',
        'letter',
        'iso',
        'slug',
        'population',
        'region_id'
    ];


}
