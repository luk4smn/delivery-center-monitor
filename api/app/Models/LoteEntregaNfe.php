<?php

namespace App\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class LoteEntregaNfe extends Model
{
    use Searchable, SoftDeletes;

    protected $table = 'lote_entrega_nfe';

    protected $fillable = [
        'nfe_id',
        'user_id',
        'delivery_lot_id',
        'obs',
        'image'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function lot()
    {
        return $this->belongsTo(LoteEntrega::class, 'delivery_lot_id', 'id');
    }

    public function nfe()
    {
        return $this->belongsTo(Nfe::class, 'nfe_id', 'id');
    }

    public function localization()
    {
        return $this->hasOne(LoteEntregaLocalizacao::class, 'delivery_lot_nfe_id', 'id');
    }

    public function setImage($uploadedFile)
    {
        $fileOriginalName = $uploadedFile->getClientOriginalName();
        $filePath = $uploadedFile->getRealPath();
        $fileContent = file_get_contents($filePath);
        $fileName = "delivery/".($this->nfe_id)."/$fileOriginalName";

        Storage::disk('public')
            ->put($fileName, $fileContent);

        $this->image = $fileName;
        $this->save();
    }

    public function getImageAttribute()
    {
        if($this->attributes['image'] && !str_contains($this->attributes['image'], 'http')){
            $url = Storage::disk('public')->url($this->attributes['image']);
            return ( env('APP_ENV') == 'production' && Str::contains($url, 'storage')) ? Str::replaceFirst('storage', 'public/storage', $url) : $url;
        }

        return $this->attributes['image'];
    }

    public function countInProgress(){
        return LoteEntregaNfe::where('delivery_lot_id', $this->delivery_lot_id)
            ->join('nfes', 'nfes.id', '=', 'lote_entrega_nfe.nfe_id')
            ->where('nfes.status', '<=', Constants::STATUS_IN_PROGRESS)
            ->count('lote_entrega_nfe.id');
    }

    public static function countNotesByStatus($status){
        return LoteEntregaNfe::join('nfes', 'nfes.id', '=', 'lote_entrega_nfe.nfe_id')
            ->where('nfes.status', $status)
            ->where('lote_entrega_nfe.user_id', auth()->user()->id)
            ->count('nfes.id');
    }





}
