<?php

namespace App\Models;

use App\Traits\Searchable;
use App\Traits\SecureDelete;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Nfe extends Model
{
    use Searchable, SoftDeletes, SecureDelete;

    protected $table = 'nfes';

    protected $fillable = [
        'id',
        'note_key',
        'note_number',
        'provider',
        'provider_doc_number',
        'client',
        'client_doc_number',
        'client_contact',
        'delivery_address',
        'tracking_code',
        'weight',
        'cubage',
        'volumes',
        'issued_at',
        'status',
        'delivery_center_id'
    ];

    protected $casts = [
        'issued_at' => 'datetime',
    ];

    public function products(){
        return $this->hasMany(NfeProduto::class, 'nfe_id');
    }

    public function events(){
        return $this->hasMany(NfeEvent::class, 'nfe_id');
    }

    public function delivery()
    {
        return $this->hasOne(LoteEntregaNfe::class, 'nfe_id');
    }

    public function deliveryCenter()
    {
        return $this->belongsTo(CentroDistribuicao::class, 'delivery_center_id', 'id');
    }

    public static function getTotalWeightCubageAndVolumes(array $notes_ids){
        return \DB::table('nfes')
            ->select(\DB::raw('ROUND(SUM(weight),2) as total_weight, ROUND(SUM(cubage),2) as total_cubage, ROUND(SUM(volumes),2) as total_volumes'))
            ->whereIn('id', $notes_ids)
            ->get()
            ->first();
    }




}
