<?php

namespace App\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use Searchable, SoftDeletes;

    protected $table = 'roles';

    protected $fillable = [
        'id',
        'name',
    ];

}
