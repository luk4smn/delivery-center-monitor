<?php

namespace App\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NfeEvent extends Model
{
    use Searchable, SoftDeletes;

    protected $table = 'nfe_events';

    protected $fillable = [
        'id',
        'event',
        'user_id'
    ];

}
