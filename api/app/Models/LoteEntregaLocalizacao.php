<?php

namespace App\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoteEntregaLocalizacao extends Model
{
    use Searchable, SoftDeletes;

    protected $table = 'lote_entrega_localizacao';

    protected $fillable = [
        'id',
        'latitude',
        'longitude',
        'user_id',
        'delivery_lot_nfe_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function delivery()
    {
        return $this->belongsTo(LoteEntregaNfe::class, 'delivery_lot_nfe_id', 'id');
    }

}
