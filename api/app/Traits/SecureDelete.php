<?php

namespace App\Traits;

use function PHPUnit\Framework\throwException;

trait SecureDelete
{
    /**
     * Delete only when there is no reference to other models.
     *
     * @param array $relations
     * @return response
     * @throws \Exception
     */
    public function secureDelete(String ...$relations)
    {
        $hasRelation = false;
        foreach ($relations as $relation) {
            if ($this->$relation()->count()) {
                $hasRelation = true;
                break;
            }
        }

        if (!$hasRelation) {
            $this->delete();
        }else{
            throw new \Exception('Erro ao deletar, item possui relação com outros elementos ativos');
        }
    }

}
