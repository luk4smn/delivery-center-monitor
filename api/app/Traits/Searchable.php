<?php

namespace App\Traits;

use App\Interfaces\SearchableInterfaces;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait Searchable {

    protected $searchable = [];

    public function scopeSearch(Builder $query, $search,  $params = []) {

        if(!empty($search)){
            $query->whereLike($params, $search)
                ->orWhereDate('created_at','=', Carbon::parse(date('Y-m-d', strtotime(str_replace('/', '-', $search)))));
        }
        return $query;
    }

}
