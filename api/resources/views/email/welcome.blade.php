<!DOCTYPE html>
<html>
<head>
    <title>Bem-vindo ao Delivery Center</title>
</head>
<body>
<h1>Bem-vindo, {{ $user->name }}!</h1>
<p>Obrigado por se juntar a nós.</p>

<p>Use a senha <b> {{ $password }}</b> para acessar a plataforma.</p>

</body>
</html>
