import Vue from "vue"
import VueRouter from "vue-router"
import LightBootstrap from "../light-bootstrap-main"
import store from "../store"
import * as types from "../store/mutation-types"

// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'
import Login from '../components/GeneralViews/Login.vue'
import Rastreamento from "../components/GeneralViews/Rastreamento";
import MainLayout from '../components/Dashboard/Layout/MainLayout.vue'
// Dashboard pages
import Overview from '../components/Dashboard/Views/Painel/Overview.vue'
import UserProfile from '../components/Dashboard/Views/UserProfile/UserProfile.vue'
import NotasFiscais from "../components/Dashboard/Views/NotasFiscais/NotasFiscais";
import MinhasEntregas from "../components/Dashboard/Views/MinhasEntregas/MinhasEntregas";
// Admin pages
import Users from '../components/Dashboard/Views/Users/Users'
import LotesEntrega from "../components/Dashboard/Views/LotesEntrega/LotesEntrega";
import CentrosDeDistribuicao from "../components/Dashboard/Views/CentrosDeDistribuicao/CentrosDeDistribuicao";
import {func} from "../store/functions";
import {getRole, getToken, getUserSessionId, username} from "../store/getters";


const verifyIfHasLoginToken = (to, from, next) => {
  const token = store.getters.getToken
  const username = store.getters.username
  const userSessionId = store.getters.getUserSessionId
  const userRoleId = store.getters.getRole

  if (token && userSessionId) {
    store.commit(types.LOGIN_SUCCESS, { token, username, userSessionId, userRoleId })
    router.push('/painel')
  } else {
    next()
  }
}

const requireAuth = (to, from, next) => {
  if (store.getters.isLoggedIn) {
    next()
  } else {
    router.push('/')
  }
}

const isAdmin = (to, from, next) => {
  if (func.isAdmin()) {
    next()
  } else {
    router.push('/')
  }
}

Vue.use(VueRouter)
Vue.use(LightBootstrap)

let index = [
  {
    path: '/',
    redirect: '/login',
    component: Login,
    children: [
      {path: 'login', name: 'Login',}
    ],
    beforeEnter: verifyIfHasLoginToken
  },
  {
    path: '/rastreamento',
    name: 'Rastreamento',
    component: Rastreamento,
  },
  {
    path: '/painel',
    component: MainLayout,
    redirect: '/painel/dashboard',
    beforeEnter: requireAuth,
    children: [
      {path: 'dashboard', name: 'Overview', component: Overview},
      {path: 'user', name: 'User', component: UserProfile},
      {path: 'notes', name: 'Notas Fiscais', component: NotasFiscais,},
      {path: 'my-deliveries', name: 'Minhas Entradas', component: MinhasEntregas,},

      {path: 'users', name: 'Users', component: Users, beforeEnter: isAdmin},
      {path: 'delivery-center', name: 'Centro De Distribuição', component: CentrosDeDistribuicao, beforeEnter: isAdmin },
      {path: 'delivery-lot', name: 'Lotes Entrega', component: LotesEntrega, beforeEnter: isAdmin },
    ]
  },
  {path: '*', component: NotFound}
];

const router = new VueRouter({routes: index});

export default router
