import * as types from "./mutation-types";
import store from "./index";

// funções auxiliares globais
export const func = {
  getStatusName: function (statusId)
  {
    switch(statusId) {
      case types.STATUS_PENDING:
        return "Aguardando Retirada";
      case types.STATUS_WITHDRAWN:
        return "Retirando do Centro de Distribuição";
      case types.STATUS_IN_PROGRESS:
        return "Em rota de entrega";
      case types.STATUS_SUSPENDED:
        return "Entrega suspensa ou com pendências";
      case types.STATUS_COMPLETED:
        return "Entrega efetuada";
      default:
        return "Sem status definido";
    }
  },

  getUserLocation: function()
  {
    if (!navigator.geolocation) {
      console.error("Seu navegador não suporta Geolocation.");
      return;
    }

    navigator.geolocation.getCurrentPosition(
      function (position) {
        return store.state.localization = {latitude: position.coords.latitude , longitude: position.coords.longitude};
        },
      function (error) {
        console.error("Erro ao obter a localização do usuário: " + error.message);
      },
      {
        enableHighAccuracy: true
      });
  },

  isAdmin: function (){
    return store.getters.getRole === types.ADMIN_ROLE
  }





}
