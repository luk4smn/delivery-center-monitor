export const LOGIN                    = 'LOGIN'
export const LOGIN_SUCCESS            = 'LOGIN_SUCCESS'
export const LOGIN_WRONG_CREDENTIALS  = 'LOGIN_WRONG_CREDENTIALS'
export const LOGIN_ERROR              = 'LOGIN_ERROR'
export const LOGOUT                   = 'LOGOUT'
export const LOADING                  = 'LOADING'
export const REFRESH_TOKEN            = 'REFRESH_TOKEN'
export const REFRESH_SUCCESS          = 'REFRESH_TOKEN_SUCCESS'
export const REFRESH_FAIL             = 'REFRESH_FAIL'

export const STATUS_PENDING      = 1
export const STATUS_WITHDRAWN    = 2
export const STATUS_IN_PROGRESS  = 3
export const STATUS_SUSPENDED    = 4
export const STATUS_COMPLETED    = 5

export const ADMIN_ROLE = 1
