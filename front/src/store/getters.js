export const isLoggedIn = state =>
  state.auth.isLoggedIn;

export const username = state =>
  state.auth.username;

export const getToken = state =>
  state.auth.token;

export const getRole = state =>
  state.auth.userRoleId;

export const getUserSessionId = state =>
  state.auth.userSessionId;

export const isRefreshed = state =>
  state.auth.refreshed

export const getLocalization = state =>
  state.localization
