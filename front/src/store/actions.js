import * as types from './mutation-types'
import * as axios from "axios"
import router from "../routes";
import store from "./index";
import {isRefreshed} from "./getters";

const login = ({ commit }, creds) => {
  commit(types.LOGIN)

  return axios({
    method: 'POST',
    url: '/api/login',
    headers: {
      'Content-Type': 'application/json'
    },
    data: JSON.stringify(creds)
  });

}

const logout = ({ commit }) => {
  let token = store.getters.getToken;

  axios({
    method: "POST",
    url: "/api/logout",
    headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    }
  }).then(
    result => {
      commit(types.LOGOUT)
      return router.push('/login')
    },
    error => {
      console.log(error)
      commit(types.LOGOUT)
      return router.push('/login')
    }
  );
}

const refreshToken = async ({commit}, data) => {
  await axios({
    method: "POST",
    url: "/api/refresh",
    headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + data.token,
      'Content-Type': 'application/json'
    }
  }).then(response => {
    commit(types.REFRESH_SUCCESS, { token: response.data.authorisation.token,})
    commit(types.LOADING, false);
    router.go(0)
  }, error => {
    commit(types.LOADING, false);

    /*if(!store.getters.isRefreshed){
      commit(types.LOGOUT)
      commit(types.REFRESH_FAIL)
      return router.push('/login')
    }*/

    console.log(error)

    commit(types.LOGOUT)
    commit(types.REFRESH_FAIL)
    return router.push('/login')

  })
}

export default {
  [types.LOGIN]: login,
  [types.LOGOUT]: logout,
  [types.REFRESH_TOKEN]: refreshToken,
}

