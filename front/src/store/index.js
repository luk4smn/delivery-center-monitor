import Vue from 'vue'
import Vuex from 'vuex'
import * as getters from './getters'
import mutations from './mutations'
import actions from './actions'
import {func} from './functions.js'
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex)
Vue.prototype.$func = func

const state = {
  auth: {
    isLoggedIn: false,
    pending: false,
    token: null,
    username: null,
    userRoleId: null,
    userSessionId: null,
    refreshed:false,
  },
  refCount: 0,
  isLoading: false,
  localization: {}
}

const options = {
  state,
  getters,
  mutations,
  actions,
  plugins: [createPersistedState()],
}

const store = new Vuex.Store(options)

export default store
