import * as types from './mutation-types'

const mutations = {
  [types.LOGIN] (state) {
    state.auth.pending = true
  },
  [types.LOGIN_SUCCESS] (state, data) {
    let token = data.token;
    let username = data.username
    let userSessionId = data.userSessionId
    let userRoleId = data.userRoleId

    state.auth.pending = false
    state.auth.isLoggedIn = true
    state.auth.token = token
    state.auth.username = username
    state.auth.userSessionId = userSessionId
    state.auth.userRoleId = userRoleId
  },
  [types.LOGIN_WRONG_CREDENTIALS] (state) {
    state.auth.pending = false
    state.auth.isLoggedIn = false
  },
  [types.LOGIN_ERROR] (state) {
    state.auth.pending = false
    state.auth.isLoggedIn = false
  },
  [types.LOGOUT] (state) {
    state.auth.token = null
    state.auth.username = null
    state.auth.userSessionId = null
    state.auth.userRoleId = null
    state.auth.isLoggedIn = false
  },
  [types.LOADING] (state, isLoading) {
    if (isLoading) {
      state.refCount++;
      state.isLoading = true;
    } else if (state.refCount > 0) {
      state.refCount--;
      state.isLoading = (state.refCount > 0);
    }
  },
  [types.REFRESH_SUCCESS] (state, data) {
    let token = data.token;

    state.auth.pending = false
    state.auth.isLoggedIn = true
    state.auth.token = token
    state.auth.refreshed = true
  },
  [types.REFRESH_FAIL] (state) {
    state.auth.refreshed = false
  },
}

export default mutations
