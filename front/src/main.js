import Vue from "vue"
import App from "./App.vue"
import router from "./routes"
import store from "./store"
import axios from "axios"
import moment from "moment";
import VueAxios from "vue-axios"
import Vuelidate from "vuelidate"
import VueTheMask from 'vue-the-mask'
import { BootstrapVue, IconsPlugin } from "bootstrap-vue"
// Import Bootstrap and BootstrapVue CSS files (order is important)
import "bootstrap/dist/css/bootstrap.css"
import "bootstrap-vue/dist/bootstrap-vue.css"

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
//axios
Vue.use(VueAxios, axios)
// Validation
Vue.use(Vuelidate);
// Mask Inputs
Vue.use(VueTheMask);

/* ========= Websocket BEGIN  ========= */
import Echo from "laravel-echo"
import IO from "socket.io-client"

window.io = IO
if (typeof io !== "undefined") {
  window.Echo = new Echo({
      broadcaster: "socket.io",
      host: window.location.hostname + ":6001"
  })
}
/*  ========= Websocket END  ========= */

Vue.filter("formatDate", function(value) {
  if (value) {
    return moment(String(value)).format("DD/MM/YYYY")
  }
});

Vue.filter("formatDateAndTime", function(value) {
  if (value) {
    return moment(String(value)).format("DD/MM/YYYY HH:mm:ss")
  }
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store,
});

