var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  VUE_APP_MAP_ACCESS_TOKEN: '"pk.eyJ1IjoibHVrNHNtbiIsImEiOiJjbGJmYmMzbjIwNGc1M3BwazQzeHMyeTNxIn0.LYNalK7YV_DlbRnkEp93UA"'
})
